import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from './services/data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Лабораторна №2';

  constructor(private router: Router, private dataService: DataService) {}

  onRestart() {
    this.router.navigate(['initial-data']);
    this.resetDataService();
  }

  onInfo() {
    this.router.navigate(['info']);
  }

  private resetDataService() {
    this.dataService.rowSize = 6;
    this.dataService.basicParameters = [0, 0, 0, 0, 0, 0];
    this.dataService.SaatiCoefficients = [0, 0, 0, 0, 0, 0];
    this.dataService.fuzzyMatrix = [];
    this.dataService.sums = [];
    this.dataService.inversedSums = [];
    this.dataService.expectedValues = [];
  }
}
