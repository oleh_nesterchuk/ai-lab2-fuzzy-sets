import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  basicParameters: number[] = [0, 0, 0, 0, 0, 0];
  SaatiCoefficients: number[] = [0, 0, 0, 0, 0, 0];
  fuzzyMatrix: number[][];
  sums: number[];
  inversedSums: number[];
  expectedValues: number[];
  rowSize: number = 6;

  constructor() { }

}
