import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitialDataComponent } from './components/initial-data/initial-data.component';
import { GraphComponent } from './components/graph/graph.component';
import { MatrixComponent } from './components/matrix/matrix.component';
import { InfoComponent } from './components/info/info.component';


const routes: Routes = [
  {path: '', redirectTo: 'initial-data', pathMatch: 'full'},
  {path: 'initial-data', component: InitialDataComponent},
  {path: 'matrix', component: MatrixComponent},
  {path: 'graph', component: GraphComponent},
  {path: 'info', component: InfoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
