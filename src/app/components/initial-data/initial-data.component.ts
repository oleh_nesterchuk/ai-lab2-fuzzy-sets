import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-initial-data',
  templateUrl: './initial-data.component.html',
  styleUrls: ['./initial-data.component.scss']
})
export class InitialDataComponent implements OnInit {

  constructor(public dataService: DataService, private router: Router) { }

  ngOnInit(): void {
  }

  onNext() {
    this.router.navigate(['matrix']);
  }

  trackByIdx(index: number, obj: any): any {
    return index;
  }

  createEmptyArray(size: number): any[] {
    let array = [];
    for (let i = 0; i < size; i++) {
      array.push(i);
    }
    return array;
  }
}
