import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as CanvasJS from '../../../assets/canvasjs.min.js';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
    let dataPoints = [];
    const rowSize = 6;
    for (let i = 0; i < rowSize; i++) {
      console.log(this.dataService);
      dataPoints.push({
        x: this.dataService.basicParameters[i],
        y: this.dataService.expectedValues[i],
      });
    }
    let chart = new CanvasJS.Chart("chartContainer", {
      // zoomEnabled: true,
      animationEnabled: true,
      exportEnabled: true,
      title: {
        text: "Графік нечіткої множини"
      },
      axisX: {
        title: "Базові величини",
      },
      axisY: {
        title: "M(X)",
      },
      data: [
        {
          type: "line",
          dataPoints: dataPoints
        }]
    }
    );

    chart.render();
  }

  onPrevious() {
    this.router.navigate(['matrix']);
  }

}
