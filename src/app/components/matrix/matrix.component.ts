import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-matrix',
  templateUrl: './matrix.component.html',
  styleUrls: ['./matrix.component.scss']
})
export class MatrixComponent implements OnInit {

  constructor(public dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.fillFuzzyMatrix();
    this.fillSums();
    this.fillInversedSums();
    this.fillExpectedValues();
  }
  
  onPrevious() {
    this.router.navigate(['initial-data']);
  }

  onNext() {
    this.router.navigate(['graph']);
  }

  private initializeFuzzyMatrix() {
    this.dataService.fuzzyMatrix = [];
    const rowSize = 6;
    for (let i = 0; i < rowSize; i++) {
      this.dataService.fuzzyMatrix[i] = [];
    }
  }

  private fillFuzzyMatrix() {
    this.initializeFuzzyMatrix();
    this.setBottomLineOfMatrix();
    this.setDiagonalElemets();
    this.setElementsBelowDiagonal();
    this.setElementsAboveDiagonal();
  }

  private setBottomLineOfMatrix() {
    for (let i = 0; i < this.dataService.rowSize; i++) {
      this.dataService.fuzzyMatrix[this.dataService.rowSize - 1][i] = 
        this.dataService.SaatiCoefficients[i];
    }
  }

  private setDiagonalElemets() {
    for (let i = 0; i < this.dataService.rowSize; i++) {
      this.dataService.fuzzyMatrix[i][i] = 1;
    }  
  }

  private setElementsBelowDiagonal() {
    const startRow = this.dataService.rowSize - 2, startColumn = 0;
    for (let i = startRow; i > 0; i--) {
      for (let j = startColumn; j <= i; ++j) {
        this.dataService.fuzzyMatrix[i][j] = this.dataService.fuzzyMatrix[i + 1][j] / 
          this.dataService.fuzzyMatrix[i + 1][i];
      }
    }
  }

  private setElementsAboveDiagonal() {
    for (let i = 0; i < this.dataService.rowSize - 1; i++) {
      for (let j = i; j < this.dataService.rowSize; j++) {
        if (i === j) continue;
        this.dataService.fuzzyMatrix[i][j] = 1 / this.dataService.fuzzyMatrix[j][i];
      }
    }
  }

  private fillSums() {
    this.dataService.sums = [];
    for (let i = 0; i < this.dataService.rowSize; i++) {
      this.dataService.sums.push(this.getColumnSum(i));
    }
  }

  private getColumnSum(column: number): number {
    let columnSum = this.dataService.fuzzyMatrix[0][column];
    for (let i = 1; i < this.dataService.rowSize; i++) {
      columnSum += this.dataService.fuzzyMatrix[i][column];
    }
    return columnSum;
  }

  private fillInversedSums() {
    this.dataService.inversedSums = [];
    for (let i = 0; i < this.dataService.rowSize; i++) {
      this.dataService.inversedSums.push(1 / this.dataService.sums[i]);
    }
  }

  private fillExpectedValues() {
    this.dataService.expectedValues = [];
    const biggestInversedValue = Math.max(...this.dataService.inversedSums);
    for (let i = 0; i < this.dataService.rowSize; i++) {
      this.dataService.expectedValues.push(this.dataService.inversedSums[i] / biggestInversedValue);
    }
  }
}
